<?php

namespace App\Services;

use App\Repositories\CarRepository;
use App\Services\Contracts\CarSharing;

class CarSharingService implements CarSharing
{
    private $repository;

    public function __construct(CarRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllCars()
    {
        return $this->repository->all();
    }

    public function getRandomCar()
    {
        return array_random($this->repository->all());
    }
}