<?php

namespace App\Providers;

use App\Repositories\CarRepository;
use App\Services\CarSharingService;
use App\Services\Contracts\CarSharing;
use App\Services\Contracts\RandomGenerator;
use App\Services\RandomGeneratorService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RandomGenerator::class, RandomGeneratorService::class);
        $this->app->bind(CarSharing::class, CarSharingService::class);
    }
}
